package com.frame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.assertj.ApplicationContextAssertProvider;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.PropertySource;

import com.frame.service.MybaService;

@SpringBootApplication
//@PropertySource(value="file:application.properties")
@ImportResource("file:**/config/spring/application-context-root.xml")
public class SpringBootTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SpringApplication application = new SpringApplication(SpringBootTest.class);
		ApplicationContext context = application.run(args);
		
		
		//test
		MybaService myba = (MybaService) context.getBean(MybaService.class);
		int a = myba.totSize();
		System.out.println(a);
	}

}

